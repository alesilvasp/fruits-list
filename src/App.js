import "./App.css";
import { useState } from "react";
import FruitList from "./components/fruitList";

function App() {
  const [fruits, setFruits] = useState([
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ]);

  const fruitsName = fruits.map((item) => {
    return item.name;
  });

  function filterRedFruits() {
    return setFruits(
      fruits.filter((item) => {
        return item.color === "red";
      })
    );
  }

  const totalPrice = fruits
    .map((item) => {
      return item.price;
    })
    .reduce((acc, item) => {
      return acc + item;
    }, 0);

  const [banana, cherry, strawberry] = fruitsName;

  return (
    <div className="App">
      <div className="App-header">
        <p>Preço total = {totalPrice}</p>

        <ul>
          <FruitList name={banana} />
          <FruitList name={cherry} />
          <FruitList name={strawberry} />
        </ul>

        <button onClick={filterRedFruits}>Mostrar as frutas vermelhas</button>
      </div>
    </div>
  );
}

export default App;
